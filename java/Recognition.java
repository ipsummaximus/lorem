import java.io.File;
import java.io.IOException;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.CV_LOAD_IMAGE_GRAYSCALE;

import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.opencv_face.EigenFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.FaceRecognizer;
import org.bytedeco.javacpp.opencv_face.FisherFaceRecognizer;
import org.bytedeco.javacpp.opencv_face.LBPHFaceRecognizer;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;

public class Recognition {
	
	final String PATH_RESOURCES = "src/main/resources/";
	final String PATH_RECOGNIZER = PATH_RESOURCES + "recognizer.yml";
    final String PATH_FACES = PATH_RESOURCES + "faces/";
    final String PATH_RESULTS_FACES = PATH_RESOURCES + "results/faces/";
    
    public Recognition() {
    	System.out.println("Recognition...");
    	
    	// Init vars
        String name = null;
        String str;
        char c;
        boolean needsTraining = false;
        Scanner reader = new Scanner(System.in);
    	
        //Set the dir with all the faces
        File[] faces = new File(PATH_FACES).listFiles();
        
        //Init Mat vars
        MatVector images = new MatVector(faces.length);
        Mat labels = new Mat(faces.length, 1, CV_32SC1);
        IntBuffer labelsBuf = labels.createBuffer();
        for (int i = 0; i < faces.length; i++ ) {
            Mat img = imread(faces[i].getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            images.put(i, img);
            labelsBuf.put(i, i);
        }

        // Different algorithms
        FaceRecognizer faceRecognizer = FisherFaceRecognizer.create();
//         FaceRecognizer faceRecognizer = EigenFaceRecognizer.create();
//         FaceRecognizer faceRecognizer = LBPHFaceRecognizer.create();

        // Check if alreay trained
        if (! (new File(PATH_RECOGNIZER).exists())) 
        	trainRecognizer(faceRecognizer, images, labels);
        else {
        	System.out.println("Loading recognizer...");
        	faceRecognizer.read(PATH_RECOGNIZER);
        }
        
        // Get list of detected faces
        File[] detectedFaces = new File(PATH_RESULTS_FACES).listFiles();
        
        //Init vars
        IntPointer label = new IntPointer(0);
        DoublePointer confidence = new DoublePointer(0);
        
        // Check each face
        for (File face : detectedFaces) {
        	//Check the face
        	Mat testFace = imread(face.getPath(), CV_LOAD_IMAGE_GRAYSCALE);
            faceRecognizer.predict(testFace, label, confidence);
            
            // The closer it is to 0.0, the better !
            System.out.println("Confidence: " + confidence.get(0));
            
            // Get name of the closest face (instead of an index)
    		name = faces[label.get(0)].getName().split("\\_")[0];

            // Get input
            do {
            	System.out.println("Is " + face.getName() + ", " + name + " ? [Y/n] (default is 'Yes')");
            	str = reader.nextLine().toLowerCase();
            	c = (str.isEmpty()) ? 'y' : str.charAt(0);
            } while (c != 'y' && c != 'n');
            
            // If wrong face found
            if (c == 'n') {
            	needsTraining = true;
            	
            	// Get input
            	System.out.println("Please enter who that is (default is 'unknown') :");
                str = reader.nextLine().toLowerCase();
                str = (str.isEmpty()) ? "unknown" : str;
                
                // Write new image
                writeNewFace(face, str, 1);
            } 
        }
        reader.close();
        
        if (needsTraining) trainRecognizer(faceRecognizer, images, labels);
    }
    
   private void writeNewFace(File face, String str, int i) {
		try {
			File f = new File(PATH_FACES + "/" + str + "_" + i + "." + face.getName().split("\\.")[1]);
			   if (f.exists()) writeNewFace(face, str, ++i);
			else
					Files.copy(face.toPath(), f.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
   }
   
   private void trainRecognizer(FaceRecognizer faceRecognizer, MatVector images, Mat labels) {
	   System.out.println("Training...");
       faceRecognizer.train(images, labels);
       faceRecognizer.save(PATH_RECOGNIZER);
   }
}

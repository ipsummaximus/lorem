import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Resize {
	public Resize() {
		System.out.println("Resizing...");
		
		File[] files = new File("src/main/resources/results/faces").listFiles();
		
        for (File file : files) {
			try {
	        	BufferedImage buff;
				buff = ImageIO.read(file);
				BufferedImage resized = resize(buff, 255, 255);
				File output = new File(file.getPath());
		        ImageIO.write(resized, "png", output);
//		        System.out.println(file.getName() + " modified");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
	
	private static BufferedImage resize(BufferedImage img, int height, int width) {
         Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
         BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
         Graphics2D g2d = resized.createGraphics();
         g2d.drawImage(tmp, 0, 0, null);
         g2d.dispose();
         return resized;
     }
}

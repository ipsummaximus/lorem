import java.io.File;

public class Main {
	public static void main(String[] args) {
        System.out.println();
        
		new Detection();
		
		System.out.println();
		
		new Resize();
		
		System.out.println();
		
		new Recognition();
		
		System.out.println();
		
		clear_folders();
	}
	
	public static void clear_folders() {
		System.out.println("Cleaning temporary folders...");
		
        File[] files0 = new File("src/main/resources/results/faces").listFiles();
        for (File file : files0) {
        	file.delete();
        }
        File[] files1 = new File("src/main/resources/results/pics").listFiles();
        for (File file : files1) {
        	file.delete();
        }
        File[] files2 = new File("src/main/resources/pics").listFiles();
        for (File file : files2) {
        	file.delete();
        }
	}
}

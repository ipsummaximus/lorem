import java.io.File;
import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class Detection {
	 
	    public Detection() {
	    	//Load library
	        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	        
	        System.out.println("Detection...");
	 
	        // Init face detector with a detection algorithm
	        CascadeClassifier faceDetector = new CascadeClassifier(new File("src/main/resources/haarcascades/haarcascade_frontalface_alt2.xml").getPath());
	        
	        // Add pictures to array
	        ArrayList<Mat>pictures = new ArrayList<Mat>(); 
	        
	        File[] files = new File("src/main/resources/pics").listFiles();
	        for (File file : files)
	        	pictures.add(Imgcodecs.imread(file.getPath()));

	        MatOfRect faceDetections = new MatOfRect();
	        
	        for (int i = 0 ; i < pictures.size(); i++)  {
		        Rect rectCrop = null; 
		        int j = 0; 
		        String s;
		        
		        // Detect faces
		        faceDetector.detectMultiScale(pictures.get(i), faceDetections);
		        System.out.println("Detected " + faceDetections.toArray().length + " faces on picture number " + (i+1) +".");
		        
		        for (Rect rect : faceDetections.toArray()) {
		            // Couper l'image
		            rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
		            
		            // Create pics
		            Mat image_roi = new Mat(pictures.get(i),rectCrop);
		            
		            // CHANGEMENT 
		            s = "src/main/resources/results/faces/face_" + i + "_" + j + ".png"; 

		            Imgcodecs.imwrite(s,image_roi);
		            
		            j++;
		        }
		        
		        // Draw rectangles around faces on the original pictures
		        for (Rect rect : faceDetections.toArray()) {
		            Imgproc.rectangle(pictures.get(i), new Point(rect.x, rect.y),  new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
		        }
		        
		        // Rewrite the original pics with the rectangles
		        
		        // CHANGEMENT 
		        String filename = "src/main/resources/results/pics/ouput_" + i + ".png";
		       
		        Imgcodecs.imwrite(filename, pictures.get(i));
	        }
	    }
}
